using Mirror;
using System;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private VRCamera _playerCamera;
    [SerializeField] private ShootController _shootController;
    [SerializeField] private float _speed;

    private Camera _camera;
    private Rigidbody _rigidbody;
    private NetworkIdentity _networkIdentity;
    private GameConsole _gameConsole;
    private DebugConsole _debugConsole;

    private void Awake()
    {
        _networkIdentity = GetComponent<NetworkIdentity>();
        _rigidbody = GetComponent<Rigidbody>();
        _camera = _playerCamera.GetComponentInChildren<Camera>();
    }

    private void Start()
    {
        if (!_networkIdentity.isLocalPlayer)
        {
            _playerCamera.enabled = false;
            foreach (Camera camera in _playerCamera.GetComponentsInChildren<Camera>())
            {
                if (camera.TryGetComponent(out AudioListener listener))
                    listener.enabled = false;
                camera.enabled = false;
            }
            return;
        }
        _playerCamera.Init();

        _gameConsole = SceneInjector.Instance.GameConsole;
        _debugConsole = SceneInjector.Instance.DebugConsole;

        _debugConsole.SetIP(IPManager.GetIP(ADDRESSFAM.IPv4));

        _gameConsole.onLurchBlocked += ChangeRotateMode;
        _gameConsole.onMove += Move;
        _gameConsole.onTapPoint += Shoot;

        _debugConsole.onClickX += () =>
        {
            FibrumController.useAntiDrift = !FibrumController.useAntiDrift;
            Debug.Log($"Use anti drift: {FibrumController.useAntiDrift}");
        };
        _debugConsole.onClickY += () =>
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            FibrumController.useCompassForAntiDrift = !FibrumController.useCompassForAntiDrift;
			_playerCamera.EnableCompass(FibrumController.useCompassForAntiDrift);
            Debug.Log($"Use compass for anti drift: {FibrumController.useCompassForAntiDrift}");
#endif
        };
    }

    private void Move()
    {
        var direction = Vector3.ProjectOnPlane(_playerCamera.vrCameraHeading.forward, Vector3.up).normalized;
        _rigidbody.velocity = direction * _speed;
    }

    private void ChangeRotateMode()
    {
        _playerCamera.lurchBlocked = !_playerCamera.lurchBlocked;
        Debug.Log($"Rotation only Y is {(_playerCamera.lurchBlocked ? "enable" : "disable")}");
    }

    private void Shoot(Vector2 point)
    {
        Ray directionShoot = _camera.ScreenPointToRay(point);
        _shootController.Shoot(directionShoot.direction);
        Debug.Log($"Shoot to direction: {directionShoot.direction}");
    }
}
