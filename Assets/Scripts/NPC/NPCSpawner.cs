﻿using Mirror;
using System.Threading.Tasks;
using UnityEngine;

public class NPCSpawner : NetworkBehaviour
{
    [SerializeField] private ObjectsPool _objectsPool;
    [SerializeField] private GameConsole _gameConsole;
    [SerializeField] private float _spawnDistance = 4f;

    private Transform _localPlayer;
    private Transform _localCamera;

    private Transform LocalPlayer
    {
        get
        {
            if (_localPlayer == null)
                _localPlayer = NetworkClient.localPlayer.transform;
            return _localPlayer;
        }
    }

    private Transform LocalCamera
    {
        get
        {
            if (_localCamera == null)
                _localCamera = LocalPlayer.GetComponentInChildren<Camera>().transform;
            return _localCamera;
        }
    }

    private void Start()
    {
        _gameConsole.onSpawn += Spawn;
    }

    private void Spawn()
    {
        Vector3 direction = Vector3.ProjectOnPlane(LocalCamera.forward, Vector3.up).normalized;
        Vector3 position = LocalPlayer.position + direction * _spawnDistance;
        SpawnNPC(position);
    }

    [Command(requiresAuthority = false)]
    private void SpawnNPC(Vector3 position)
    {
        var npc = _objectsPool.GetObject(EObjectPoolType.NPC);
        npc.transform.position = position;
        npc.GetComponent<NPCController>().position = position;
    }
}
