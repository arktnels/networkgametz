﻿using Mirror;
using UnityEngine;

public class NPCController : NetworkBehaviour, IPooledObject
{
    [SerializeField] private float _timeoutChangeMin = 1f;
    [SerializeField] private float _timeoutChangeMax = 3f;
    [SerializeField] private float _speedMin = 1f;
    [SerializeField] private float _speedMax = 3f;
    [SerializeField] private float _rotateSpeed = 10f;
    [SerializeField] private EObjectPoolType _type;

    [SyncVar(hook = nameof(SetPosition))][HideInInspector] public Vector3 position;
    [SyncVar(hook = nameof(SetRotation))] private Quaternion _rotation;
    private Rigidbody _rigidbody;

    private Vector3 _direction;
    private float _speed;

    public EObjectPoolType Type => _type;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    [Server]
    private void Start()
    {
        ChangeDirection();
        ChangeSpeed();
    }

    private void FixedUpdate()
    {
        if (isServer)
        {
            Move();
            Rotate();
        }
    }

    [Server]
    private void Move()
    {
        _rigidbody.velocity = _direction * _speed;
        position = transform.position;
    }

    [Server]
    private void Rotate()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_direction), _rotateSpeed * Time.fixedDeltaTime);
        _rotation = transform.rotation;
    }

    private void ChangeDirection()
    {
        Vector2 dir = Random.insideUnitCircle.normalized;
        _direction = new Vector3(dir.x, 0f, dir.y);

        float timeWaiting = Random.Range(_timeoutChangeMin, _timeoutChangeMax);
        Invoke(nameof(ChangeDirection), timeWaiting);
    }

    private void ChangeSpeed()
    {
        _speed = Random.Range(_speedMin, _speedMax);

        float timeWaiting = Random.Range(_timeoutChangeMin, _timeoutChangeMax);
        Invoke(nameof(ChangeSpeed), timeWaiting);
    }

    private void SetPosition(Vector3 _, Vector3 newPosition)
    {
        transform.position = newPosition;
    }

    private void SetRotation(Quaternion _, Quaternion newRotation)
    {
        transform.rotation = newRotation;
    }
}
