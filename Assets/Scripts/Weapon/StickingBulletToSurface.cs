﻿using System;
using UnityEngine;

public class StickingBulletToSurface : MonoBehaviour, IPooledObject
{
    [SerializeField] private Rigidbody _rb;
    [SerializeField] private SphereCollider _myCollider;
    [SerializeField] private EObjectPoolType _type;

    public Action<Collision> OnHit;

    public EObjectPoolType Type => _type;

    public void Create(Action<Collision> onHit)
    {
        OnHit = onHit;
        _rb.isKinematic = false;
        _myCollider.isTrigger = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        _rb.isKinematic = true;
        _myCollider.isTrigger = true;

        if (collision.collider.attachedRigidbody != null)
        {
            transform.SetParent(collision.collider.transform);
            return;
        }

        OnHit?.Invoke(collision);
    }
}
