﻿using Mirror;
using UnityEngine;

public class ShootController : NetworkBehaviour
{
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private Transform _arrowPrefab;
    [SerializeField] private float _maxSpeed = 10;

    private ObjectsPool _objectsPool;

    private void Awake()
    {
        _objectsPool = FindObjectOfType<ObjectsPool>();
    }

    [Command(requiresAuthority = false)]
    public void Shoot(Vector3 direction)
    {
        var arrow = _objectsPool.GetObject(EObjectPoolType.Arrow);
        arrow.transform.position = _spawnPoint.position;
        arrow.transform.forward = direction;
        arrow.GetComponent<StickingBulletToSurface>().Create(CollisionEnter);
        Rigidbody rigidbody = arrow.GetComponent<Rigidbody>();
        rigidbody.AddForce(direction * _maxSpeed, ForceMode.Impulse);
    }

    private void CollisionEnter(Collision collision)
    {
        _objectsPool.ReleaseObject(collision.contacts[0].thisCollider.gameObject);
    }
}
