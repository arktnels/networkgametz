﻿using UnityEngine;

public class BulletRotation : MonoBehaviour
{
    private Rigidbody _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        transform.forward = Vector3.Slerp(transform.forward, _rb.velocity.normalized, Time.fixedDeltaTime);
    }
}
