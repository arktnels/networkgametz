﻿using Mirror;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsPool : NetworkBehaviour
{
    [Serializable]
    public struct ObjectInfo
    {
        public EObjectPoolType type;
        public GameObject prefab;
        public int startCount;
    }

    [SerializeField] private List<ObjectInfo> _objectsInfo;
    private HashSet<GameObject> _inSceneObjects;
    [SyncVar] private int _npcCount = 0;

    public int NPCCount
    {
        get => _npcCount;
        private set => _npcCount = value;
    }

    private void Start()
    {
        if (isServer)
            InitPool();
    }

    private void InitPool()
    {
        _inSceneObjects = new HashSet<GameObject>();
    }

    private GameObject InstantiateObject(EObjectPoolType type, Transform parent)
    {
        var go = Instantiate(_objectsInfo.Find(x => x.type == type).prefab, parent);
        NetworkServer.Spawn(go);
        go.SetActive(false);
        return go;
    }

    public GameObject GetObject(EObjectPoolType type)
    {
        var obj = InstantiateObject(type, null);

        obj.SetActive(true);
        _inSceneObjects.Add(obj);
        if (type == EObjectPoolType.NPC)
            NPCCount++;

        return obj;
    }

    [Command(requiresAuthority = false)]
    public void ReleaseObject(GameObject obj)
    {
        var type = obj.GetComponent<IPooledObject>().Type;
        _inSceneObjects.Remove(obj);
        Destroy(obj);
        if (type == EObjectPoolType.NPC)
            NPCCount--;
    }

    [Command(requiresAuthority = false)]
    public void ReleaseAllObjects()
    {
        Queue<GameObject> objects = new Queue<GameObject>(_inSceneObjects);
        while (objects.Count > 0)
            ReleaseObject(objects.Dequeue());
    }
}
