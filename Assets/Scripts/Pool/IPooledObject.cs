﻿public interface IPooledObject
{
    public EObjectPoolType Type { get; }
}
