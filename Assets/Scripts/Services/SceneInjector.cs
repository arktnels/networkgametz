﻿using UnityEngine;

public class SceneInjector : MonoBehaviour
{
    [SerializeField] private GameConsole _gameConsole;
    [SerializeField] private DebugConsole _debugConsole;

    public static SceneInjector Instance {  get; private set; }

    public GameConsole GameConsole => _gameConsole;
    public DebugConsole DebugConsole => _debugConsole;

    private void Awake()
    {
        Instance = this;
    }
}
