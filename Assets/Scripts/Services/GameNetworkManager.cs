using Mirror;
using UnityEngine;

public class GameNetworkManager : NetworkManager
{
    public override void Start()
    {
        base.Start();
        Application.targetFrameRate = 120;
    }

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        base.OnServerAddPlayer(conn);

        NetworkServer.SpawnObjects();
    }
}
