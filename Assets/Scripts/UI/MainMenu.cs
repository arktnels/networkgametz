using Mirror;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private NetworkManager _networkManager;

    [SerializeField] private TMP_InputField _ipInputField;
    [SerializeField] private TMP_InputField _portInputField;
    [SerializeField] private Button _connectButton;
    [SerializeField] private Button _startButton;

    private void Awake()
    {
        _connectButton.onClick.AddListener(ConnectGame);
        _startButton.onClick.AddListener(StartGame);
    }

    private async void StartGame()
    {
        SettingServerData();

        var progress = SceneManager.LoadSceneAsync(1);
        while (!progress.isDone)
            await Task.Delay((int)(Time.deltaTime * 1000));
        _networkManager.StartHost();
    }

    private async void ConnectGame()
    {
        SettingServerData();

        var progress = SceneManager.LoadSceneAsync(1);
        while (!progress.isDone)
            await Task.Delay((int)(Time.deltaTime * 1000));
        _networkManager.StartClient();
    }

    private void SettingServerData()
    {
        _networkManager.networkAddress = _ipInputField.text;
        if (Transport.active is PortTransport portTransport)
        {
            if (ushort.TryParse(_portInputField.text, out ushort port))
                portTransport.Port = port;
        }
    }
}
