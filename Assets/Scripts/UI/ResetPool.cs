using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class ResetPool : MonoBehaviour
{
    [SerializeField] private ObjectsPool _pool;
    [SerializeField] private Button _resetPoolButton;

    private Camera _playerCamera;
    private Camera PlayerCamera
    {
        get
        {
            if (_playerCamera == null)
                _playerCamera = NetworkClient.localPlayer.GetComponentInChildren<Camera>();
            return _playerCamera;
        }
    }

    private void Awake()
    {
        _resetPoolButton.onClick.AddListener(ResetPools);
    }

    private void ResetPools()
    {
        _pool.ReleaseAllObjects();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = PlayerCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.collider.TryGetComponent(out Button button))
                {
                    button.onClick?.Invoke();
                    Debug.Log($"Tap to: {hit.collider.name}");
                }
            }
        }
    }
}
