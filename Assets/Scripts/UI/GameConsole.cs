using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameConsole : MonoBehaviour
{
    [SerializeField] private ObjectsPool _pool;
    [SerializeField] private HoldButton _moveButton;
    [SerializeField] private Button _lurchBlockedButton;
    [SerializeField] private TouchDetectZone _touchDetectZone;
    [SerializeField] private Button _spawnNpcButton;
    [SerializeField] private TMP_Text _infoText;

    public event Action onMove;
    public event Action onLurchBlocked;
    public event Action<Vector2> onTapPoint;
    public event Action onSpawn;

    private float _time;
    private int _fps;

    private void Awake()
    {
        _lurchBlockedButton.onClick.AddListener(() => onLurchBlocked?.Invoke());

        _moveButton.onHold += () => onMove?.Invoke();

        _touchDetectZone.onTapPoint += point => onTapPoint?.Invoke(point);

        _spawnNpcButton.onClick.AddListener(() => onSpawn?.Invoke());
    }

    private void Update()
    {
        _time += Time.deltaTime;
        _fps++;

        if (_time > 1f)
        {
            _time = 0f;
            _infoText.text = $"FPS: {_fps}\nNPC: {_pool.NPCCount}";
            _fps = 0;
        }
    }
}
