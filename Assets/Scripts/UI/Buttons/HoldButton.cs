using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HoldButton : Button
{
    private bool _isHold;

    public event Action onHold;

    private void Update()
    {
        if (_isHold)
            onHold?.Invoke();
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        _isHold = true;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        _isHold = false;
    }
}
