﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TouchDetectZone : MonoBehaviour, IPointerClickHandler
{
    public event Action<Vector2> onTapPoint;

    public void OnPointerClick(PointerEventData eventData)
    {
        onTapPoint?.Invoke(eventData.position);
    }
}
