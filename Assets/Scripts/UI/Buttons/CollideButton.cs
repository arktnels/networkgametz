using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public class CollideButton : Button
{
    protected override void Awake()
    {
        base.Awake();
        var image = targetGraphic.rectTransform;
        GetComponent<BoxCollider>().size = new Vector3(image.rect.size.x, image.rect.size.y, 0.005f);
    }
}
