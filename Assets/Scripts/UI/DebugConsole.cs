using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum PriorityAxys
{
    X,
    Y,
    Z
}

public class DebugConsole : MonoBehaviour
{
    [SerializeField] private TMP_Text _coordinateText;
    [SerializeField] private TMP_Text _ipText;
    [SerializeField] private Button _xSelectButton;
    [SerializeField] private Button _ySelectButton;
    [SerializeField] private Button _zSelectButton;
    [SerializeField] private Button _signChangeButton;

    public event Action onClickX;
    public event Action onClickY;

    private PriorityAxys _priorityAxys = PriorityAxys.X;
    private bool _isPositive;


    public PriorityAxys PriorityAxys => _priorityAxys;
    public bool IsPositive => _isPositive;

    private void Awake()
    {
        _xSelectButton.onClick.AddListener(() =>
        {
            onClickX?.Invoke();
            //_priorityAxys = PriorityAxys.X;
        });
        _ySelectButton.onClick.AddListener(() =>
        {
            onClickY?.Invoke();
            //_priorityAxys = PriorityAxys.Y;
        });
        _zSelectButton.onClick.AddListener(() =>
        {
            _priorityAxys = PriorityAxys.Z;
        });
        _signChangeButton.onClick.AddListener(() =>
        {
            _isPositive = !_isPositive;
        });
    }

    public void SetCoordinate(Vector3 value)
    {
        _coordinateText.text = $"X: {value.x}, Y: {value.y}, Z: {value.z}";
    }

    public void SetIP(string value)
    {
        _ipText.text = value;
    }
}
